package com.ramosvji.client.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.ramosvji.client.config.ConfigSecurity;
import com.ramosvji.client.entity.Client;
import com.ramosvji.client.entity.Clients;
import com.ramosvji.client.repository.ClientRepository;
import com.ramosvji.client.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {
	@Autowired
	private ClientRepository clientRepository;
	
	@Autowired
	private ConfigSecurity configSecurity;

	@Override
	public Client getClientByUsername(final String username) {
		return clientRepository.findByUsername(username);
	}

	@Override
	public Clients getClients() {
		Clients clients = new Clients();
		clients.setClients(clientRepository.findAll());
		return clients;
	}

	@Override
	public List<Client> getAllClients() {
		return clientRepository.findAll();
	}

	@Override
	public void saveClient(final Client client) {
		if(client != null ) {
			final String passwordEncoded = configSecurity.passwordEncode(client.getPassword());
			client.setPassword(passwordEncoded);
		}
		clientRepository.save(client);
	}

}

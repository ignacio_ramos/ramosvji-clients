package com.ramosvji.client.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.ramosvji.client.entity.Client;
import com.ramosvji.client.entity.Clients;

@Service
public interface ClientService {
	
	public Client getClientByUsername(final String username);
	
	public Clients getClients();
	
	public List<Client> getAllClients();
	
	public void saveClient(final Client client);

}

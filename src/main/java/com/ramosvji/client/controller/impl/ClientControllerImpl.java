package com.ramosvji.client.controller.impl;

import java.util.List;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.ramosvji.client.controller.ClientController;
import com.ramosvji.client.dto.ClientDtoRequest;
import com.ramosvji.client.dto.ClientDtoResponse;
import com.ramosvji.client.dto.ClientsDtoResponse;
import com.ramosvji.client.entity.Client;
import com.ramosvji.client.service.ClientService;

@RestController
@RequestMapping(path="/ramosvji")
public class ClientControllerImpl implements ClientController {
	@Autowired
	ClientService clientService;
	
	@Autowired
    private ModelMapper modelMapper;

	@GetMapping(path="/v01/clients/{username}")
	@Override
	public ResponseEntity<ClientDtoResponse> getClientByUsername(final @PathVariable("username") String username) {
		ClientDtoResponse dto = modelMapper.map(clientService.getClientByUsername(username), ClientDtoResponse.class);
		return new ResponseEntity<ClientDtoResponse>(dto, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping(path="/v01/clientl")
	@Override
	public ResponseEntity<ClientsDtoResponse> getClients() {
		ClientsDtoResponse dto = modelMapper.map(clientService.getClients(),ClientsDtoResponse.class);
		return new ResponseEntity<ClientsDtoResponse>(dto, new HttpHeaders(), HttpStatus.OK);
	}

	@GetMapping(path="/v01/clients")
	@Override
	public ResponseEntity<List<Client>> getAllClients() {
		return new ResponseEntity<List<Client>>(clientService.getAllClients(), new HttpHeaders(), HttpStatus.OK);
	}

	@PostMapping(path="/v01/clients")
	@Override
	public ResponseEntity<Void> saveClient(final @RequestBody ClientDtoRequest client) {
		
		Client entity = modelMapper.map(client, Client.class);
		clientService.saveClient(entity);
		
		return new ResponseEntity<Void>(null, new HttpHeaders(), HttpStatus.OK);
	}
	
}

package com.ramosvji.client.controller;

import java.util.List;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.ramosvji.client.dto.ClientDtoRequest;
import com.ramosvji.client.dto.ClientDtoResponse;
import com.ramosvji.client.dto.ClientsDtoResponse;
import com.ramosvji.client.entity.Client;

@RestController
public interface ClientController {
	
	@GetMapping(path="/v01/client/{username}")
	public ResponseEntity<ClientDtoResponse> getClientByUsername(final @PathVariable("username") String username);
	
	@GetMapping(path="/v01/clientsk/")
	public ResponseEntity<ClientsDtoResponse> getClients();
	
	@GetMapping(path="/v01/clients/")
	public ResponseEntity<List<Client>> getAllClients();
	
	@PostMapping(path="/v01/clients/")
	public ResponseEntity<?> saveClient(final @RequestBody ClientDtoRequest client);

}

package com.ramosvji.client.dto;

import java.io.Serializable;
import java.util.List;

public class ClientDtoRequest implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private String name;
	private String lastname;
	private String username;
	private String email;
	private boolean enable;
	private String password;
	private List<RolDtoRequest> roles;
	
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getLastname() {
		return lastname;
	}
	public void setLastname(String lastname) {
		this.lastname = lastname;
	}
	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public boolean isEnable() {
		return enable;
	}
	public void setEnable(boolean enable) {
		this.enable = enable;
	}
	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}
	public List<RolDtoRequest> getRoles() {
		return roles;
	}
	public void setRoles(List<RolDtoRequest> roles) {
		this.roles = roles;
	}
}

package com.ramosvji.client.dto;

import java.io.Serializable;
import java.util.List;

public class ClientsDtoResponse implements Serializable {
	private static final long serialVersionUID = 1L;
	
	private List<ClientDtoResponse> clients;

	public List<ClientDtoResponse> getClients() {
		return clients;
	}

	public void setClients(List<ClientDtoResponse> clients) {
		this.clients = clients;
	}
}

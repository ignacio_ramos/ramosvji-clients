package com.ramosvji.client.config;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Component;

@Component
public class ConfigSecurity {
	
	public String passwordEncode(String password) {
		String passwordEncoded;
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		
		passwordEncoded = encoder.encode(password);
		
		return passwordEncoded;
	}

}

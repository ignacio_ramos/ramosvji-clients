package com.ramosvji.client.repository;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

import com.ramosvji.client.entity.Client;

@Repository
public interface ClientRepository extends MongoRepository<Client,String> {
	
	public Client findByUsername(final String username);

}
